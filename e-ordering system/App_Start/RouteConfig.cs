﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace e_ordering_system
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
             name: "Logout",
             url: "Logout",
             defaults: new { controller = "Default", action = "Logout", id = UrlParameter.Optional }
         );

            routes.MapRoute(
              name: "Default",
              url: "{controller}/{action}/{id}",
              defaults: new { controller = "Default", action = "Index", id = UrlParameter.Optional }
          );
            routes.MapRoute(
               name: "User",
               url: "User",
               defaults: new { controller = "User", action = "Index", id = UrlParameter.Optional }
           );
            routes.MapRoute(
             name: "AutoMap",
             url: "{controller}/{action}/{id}",
             defaults: new { controller = "{controller}", action = "{action}", id = UrlParameter.Optional }
         );
        }
    }
}
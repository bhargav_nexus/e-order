﻿using e_ordering_system.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace e_ordering_system.Controllers
{
    public class KitchenStaffController : Controller
    {
        //
        // GET: /KitchenStaff/
        ServiceModel srvcmodel = new ServiceModel();
        GeneralModel gnralmdl = new GeneralModel();
        public ActionResult Index()
        {
            List<order> orderlist = new List<order>();
            MembershipUser usr = Membership.GetUser();
            List<Guid> servicelist = srvcmodel.getkitchenstaffrservicelist();
            foreach (Guid service in servicelist)
            {
                order ordermodel = new order();
                ordermodel.serviceid = service;
                //ordermodel.assignedtables = srvcmodel.getassignedtables(service);
                ordermodel.servicelist = srvcmodel.getservicequeforservice(service);
                orderlist.Add(ordermodel);
            }
            return View(orderlist);
        }

    }
}

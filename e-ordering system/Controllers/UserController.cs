﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_ordering_system.Models;
using System.Web.Security;

namespace e_ordering_system.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        UserModel usermodel = new UserModel();
        public ActionResult Index()
        {
            List<Prop_UserModel> userlist = usermodel.getallusers();
            return View(userlist);
        }

        public ActionResult AddNew()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddNew(Prop_UserModel usrmodel)
        {
            Membership.CreateUser(usrmodel.username, "admin@123");
            MembershipUser user = Membership.GetUser(usrmodel.username);
            Roles.AddUserToRole(usrmodel.username, usrmodel.userrole.ToLower());
            return RedirectToAction("Index", "User");
        }
    }
}

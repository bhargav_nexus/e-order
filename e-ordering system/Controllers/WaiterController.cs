﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_ordering_system.Models;
using System.Web.Security;

namespace e_ordering_system.Controllers
{
    public class WaiterController : Controller
    {
        //
        // GET: /Waiter/
        ServiceModel srvcmodel = new ServiceModel();
        GeneralModel gnralmdl = new GeneralModel();
        public ActionResult Index()
        {
            List<order> orderlist = new List<order>();
            MembershipUser usr = Membership.GetUser();
            List<Guid> servicelist = srvcmodel.getwaiterservicelist(new Guid(usr.ProviderUserKey.ToString()));
            foreach (Guid service in servicelist)
            {
                order ordermodel = new order();
                ordermodel.serviceid = service;
                ordermodel.assignedtables = srvcmodel.getassignedtables(service);
                ordermodel.servicelist = srvcmodel.getservicequeforservice(service);
                orderlist.Add(ordermodel);
            }
            return View(orderlist);
        }

        public ActionResult NewService()
        {
            List<menu> menulist = new Dishmodel().bindmenulist();
            return View(menulist);
        }

        [HttpPost]
        public ActionResult NewService(int dsh_id)
        {
            List<menu> menulist = new Dishmodel().bindmenulist();
            return View(menulist);
        }

        public JsonResult adddishtoservicequeue(prop_servicequeue model)
        {
            MembershipUser usr = Membership.GetUser();
            model.sq_username = new Guid(usr.ProviderUserKey.ToString());
            int serqueitem = srvcmodel.insertnewservicequeueitem(model);
            return Json(serqueitem);
        }
    }
}

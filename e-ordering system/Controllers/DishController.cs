﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_ordering_system.Models;

namespace e_ordering_system.Controllers
{
    public class DishController : Controller
    {
        //
        // GET: /Dish/
        Dishmodel dshmdl = new Dishmodel();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dishcatagery()
        {
            List<string> dishcatageries = dshmdl.getalldishcatagery();
            return View(dishcatageries);
        }

        public ActionResult NewCatagery()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewCatagery(string catagery)
        {
            int insertedid=dshmdl.insertdishcatagery(catagery);
            return RedirectToAction("Dishcatagery","Dish");
        }

        public ActionResult DeleteCatgery(string catagery)
        {
            dshmdl.deletedishcatagery(catagery);
            return RedirectToAction("Dishcatagery","Dish");
        }

        [HttpGet]
        public ActionResult DishItem()
        {
            List<Prop_dishitems> items = dshmdl.getalldishitems();
            return View(items);
        }

        [HttpGet]
        public ActionResult NewItem()
        {
            List<KeyValuePair<int, string>> customizationlist = new ServiceModel().dishcustomizationlist();
            return View(customizationlist);
        }
        [HttpPost]
        public ActionResult NewItem(Prop_dishitems itemmodel)
        {
            int insertedid = dshmdl.insertdishitem(itemmodel);
            foreach(string ct in itemmodel.dsh_custtype)
            {
                int mapinsid = dshmdl.insertdishcustomization(insertedid, Convert.ToInt32(ct));
            }
            return RedirectToAction("DishItem","Dish");
        }

        public ActionResult DishItemEdit(int id)
        {
            // code for edit
            return RedirectToAction("DishItem", "Dish");
        }

        public ActionResult DishItemDelete(int id)
        {
           int dltid = dshmdl.deletedishitem(id);
            return RedirectToAction("DishItem", "Dish");
        }

        public ActionResult ItemUpdate(int id)
        {
            Prop_dishitems item = dshmdl.getdishitem(id);
            ViewData["dshname"] = item.dsh_name;
            ViewData["dshid"] = id;
            ViewData["dshprise"] = item.dsh_prise;
            ViewData["custyp"] = item.dsh_custtype;
            List<KeyValuePair<int, string>> customizationlist = new ServiceModel().dishcustomizationlist();
            return View(customizationlist);
        }
        [HttpPost]
        public ActionResult ItemUpdate(Prop_dishitems items)
        {
            int updatedid = dshmdl.updatedishitem(items);
            foreach (string ct in items.dsh_custtype)
            {
                int mapinsid = dshmdl.insertdishcustomization(updatedid, Convert.ToInt32(ct));
            }
            return RedirectToAction("DishItem", "Dish");
        }
    }
}

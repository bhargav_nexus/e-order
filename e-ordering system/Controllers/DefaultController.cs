﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace e_ordering_system.Controllers
{
    public class DefaultController : Controller
    {
        //
        // GET: /Default/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string UserName)
        {
            if (Membership.ValidateUser(UserName, "admin@123"))
            {
                MembershipUser user = Membership.GetUser(UserName, true);
                Session["UserGuid"] = user.ProviderUserKey;
                string providerid = user.ProviderUserKey.ToString();
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                  1,                                     // ticket version
                                 UserName,                        // authenticated username
                                  DateTime.Now,                          // issueDate
                                  DateTime.Now.AddMinutes(30),           // expiryDate
                                  false,                          // true to persist across browser sessions
                                  providerid,                              // can be used to store additional user data
                                  FormsAuthentication.FormsCookiePath);  // the path for the cookie
                // Encrypt the ticket using the machine key
                string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                // Add the cookie to the request to save it
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                cookie.HttpOnly = true;
                Response.Cookies.Add(cookie);

                
                if (Roles.IsUserInRole(user.UserName,"waiter"))
                {
                    return RedirectToAction("Index", "Waiter");
                }
                else if (Roles.IsUserInRole(user.UserName, "kitchenstaff"))
                {
                    return RedirectToAction("Index", "KitchenStaff");
                }
                else if (Roles.IsUserInRole(user.UserName, "billstaff"))
                {
                    return RedirectToAction("Index", "Home");
                }
                else if (Roles.IsUserInRole(user.UserName, "administrator"))
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewData["loginerror"] = "this user is out of staff";
                    return View();
                }
            }
            else
            {
                ViewData["loginerror"] = "unable to authenticate user";
                return View();
            }
        }
        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index","Default");
        }
    }
}

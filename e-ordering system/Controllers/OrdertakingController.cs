﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using e_ordering_system.Models;

namespace e_ordering_system.Controllers
{
    public class OrdertakingController : Controller
    {
        //
        // GET: /Ordertaking/
        ServiceModel srvcmodel = new ServiceModel();
        GeneralModel gnrlmodel = new GeneralModel();

        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public JsonResult createnewservice()
        {
            Guid userid = new Guid();
            int serid = srvcmodel.insertnewservice(userid);
            Guid serviceUniqueid = srvcmodel.getserviceuniqueid(serid);
            srvcmodel.mapservicetostatus(serviceUniqueid,1,userid);
            return Json(serviceUniqueid);
        }

        [HttpPost]
        public JsonResult maptabletoservice(Guid serviceid, int tableid)
        {
            Guid userid = new Guid();
            srvcmodel.maptabletoservice(serviceid, tableid);
            srvcmodel.mapservicetostatus(serviceid, 2, userid);
            return Json(1);
        }
        //================== Make Menu =======================
        // bind tables to dropdown
        [HttpPost]
        public JsonResult bindtable()
        {
            List<Prop_Bindmodel> listitems = gnrlmodel.bindtablelist();
            return Json(listitems);
        }

        // bind dishcatagery to dropdown
        [HttpPost]
        public JsonResult binddishcatagery()
        {
            List<Prop_Bindmodel> listitems = gnrlmodel.bindcatagerylist();
            return Json(listitems);
        }

        // bind dishitems as per catagery to dropdown
        [HttpPost]
        public JsonResult binddishitems(int dishcatagery)
        {
            List<Prop_Bindmodel> listitems = gnrlmodel.binddishitemlist(dishcatagery);
            return Json(listitems);
        }

        // bind dish customization as per item to dropdown and customization value
        [HttpPost]
        public JsonResult binddishcustomization(int dishid)
        {
            List<Prop_Bindmodel> listitems = gnrlmodel.binddishitemcustomization(dishid);
            return Json(listitems);
        }

        [HttpPost]
        public JsonResult binddishcustomizationvalue()
        {
            List<Prop_Bindmodel> listitems = gnrlmodel.binddishitemcustomizationvalue();
            return Json(listitems);
        }

        //================== Add Dish to Service Queue =======================
        public JsonResult adddishtoservicequeue(prop_servicequeue model)
        {
            Guid userid = new Guid();
            model.sq_username = userid;
            int serqueitem =srvcmodel.insertnewservicequeueitem(model);
            return Json(serqueitem);
        }

        [HttpPost]
        public ActionResult publishorder(string serviceid)
        {
            List<serviceitem> items = srvcmodel.getservicequeforservice(new Guid(serviceid));
            order postorder = new order();
            postorder.serviceid = new Guid(serviceid);
            postorder.servicelist = items;
            // send postorder to kitchen staff
            return RedirectToAction("Index", "Ordertaking");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace e_ordering_system.Models
{
    public class DataConnection
    {
        private SqlConnection _Connection;

        public SqlConnection Connection
        {
            get 
            {
                if (_Connection == null)
                    {
                        _Connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()); 
                    }

                if (_Connection.State == ConnectionState.Closed)
                {
                    _Connection.Open(); 
                }
                return _Connection; 
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace e_ordering_system.Models
{
    public class Prop_Bindmodel
    {
        public int modelid { get; set; }
        public string modelitem { get; set; }
    }

    public class GeneralModel : DataConnection
    {
        public List<Prop_Bindmodel> bindtablelist()
        {
            List<Prop_Bindmodel> itemlist = new List<Prop_Bindmodel>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetTableForDrop", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                using(SqlDataReader rdr= cmd.ExecuteReader())
                {
                    while(rdr.Read())
                    {
                        Prop_Bindmodel model = new Prop_Bindmodel();
                        model.modelid = Convert.ToInt32(rdr["tbl_id"]);
                        model.modelitem = rdr["tbl_number"].ToString();
                        itemlist.Add(model);
                    }
                    rdr.Close();
                }
            }
            return itemlist;
        }

        public List<Prop_Bindmodel> bindcatagerylist()
        {
            List<Prop_Bindmodel> itemlist = new List<Prop_Bindmodel>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetCatageryForDrop", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        Prop_Bindmodel model = new Prop_Bindmodel();
                        model.modelid = Convert.ToInt32(rdr["dc_id"]);
                        model.modelitem = rdr["dc_category"].ToString();
                        itemlist.Add(model);
                    }
                    rdr.Close();
                }
            }
            return itemlist;
        }

        public List<Prop_Bindmodel> binddishitemlist(int catageryid)
        {
            List<Prop_Bindmodel> itemlist = new List<Prop_Bindmodel>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetDishItemForDrop", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@catageryid", catageryid);
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        Prop_Bindmodel model = new Prop_Bindmodel();
                        model.modelid = Convert.ToInt32(rdr["dsh_id"]);
                        model.modelitem = rdr["dsh_name"].ToString();
                        itemlist.Add(model);
                    }
                    rdr.Close();
                }
            }
            return itemlist;
        }

        public List<Prop_Bindmodel> binddishitemcustomization(int itemid)
        {
            List<Prop_Bindmodel> itemlist = new List<Prop_Bindmodel>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetDishCustomizationForDrop", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@dishid", itemid);
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        Prop_Bindmodel model = new Prop_Bindmodel();
                        model.modelid = Convert.ToInt32(rdr["cst_type_id"]);
                        model.modelitem = rdr["cst_type"].ToString();
                        itemlist.Add(model);
                    }
                    rdr.Close();
                }
            }
            return itemlist;
        }

        public List<Prop_Bindmodel> binddishitemcustomizationvalue()
        {
            List<Prop_Bindmodel> itemlist = new List<Prop_Bindmodel>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetDishCustomizationvalueForDrop", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        Prop_Bindmodel model = new Prop_Bindmodel();
                        model.modelid = Convert.ToInt32(rdr["cst_val_id"]);
                        model.modelitem = rdr["cst_value"].ToString();
                        itemlist.Add(model);
                    }
                    rdr.Close();
                }
            }
            return itemlist;
        }
    }
}
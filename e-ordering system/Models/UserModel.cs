﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace e_ordering_system.Models
{
    public class Prop_UserModel
    {
        public string username { get; set; }
        public string userrole { get; set; }
    }

    public class UserModel : DataConnection
    {
        public List<Prop_UserModel> getallusers()
        {
            List<Prop_UserModel> users = new List<Prop_UserModel>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetUserList", Connection))
            {
                using(SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while(rdr.Read())
                    {
                        Prop_UserModel model = new Prop_UserModel();
                        model.username = rdr["UserName"].ToString();
                        model.userrole = rdr["RoleName"].ToString();
                        users.Add(model);
                    }
                }
            }
            return users;
        }
    }
}
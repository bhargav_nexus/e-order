﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace e_ordering_system.Models
{
    public class prop_servicequeue
    {
        public Guid serviceid { get; set; }
        public int sq_item { get; set; }
        public int sq_quentety { get; set; }
        public string sq_note { get; set; }
        public Guid sq_username { get; set; }
        public string[] dsh_custtype { get; set; }
        public string[] dshcustomizationval { get; set; }
    }

    public class serviceitem
    {
        public string dishname { get; set; }
        public int dishquentety { get; set; }
        public string extranote { get; set; }
        public int sqid { get; set; }
        public object cusomization { get; set; }
    }

    public class order
    {
        public Guid serviceid { get; set; }
        public List<string> assignedtables { get; set; }
        public List<serviceitem> servicelist { get; set; }
    }

    public class ServiceModel : DataConnection
    {
        public List<KeyValuePair<int, string>> dishcustomizationlist()
        {
            List<KeyValuePair<int, string>> tables = new List<KeyValuePair<int, string>>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetCustomizationList", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        tables.Add(new KeyValuePair<int, string>(Convert.ToInt32(rdr["cst_type_id"]), rdr["cst_type"].ToString()));
                    }
                    rdr.Close();
                }
            }
            return tables;
        }

        // create new service
        public int insertnewservice(Guid svc_byusr)
        {
            int serviceuniqueid = 0;
            using (SqlCommand cmd = new SqlCommand("Sp_insertNewService", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@uniqueid", SqlDbType.Int);
                cmd.Parameters["@uniqueid"].Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@byuser", svc_byusr);
                cmd.ExecuteNonQuery();
                serviceuniqueid = Convert.ToInt32(cmd.Parameters["@uniqueid"].Value);
            }
            return serviceuniqueid;
        }

        // get uniqueid of service
        public Guid getserviceuniqueid(int svcid)
        {
            Guid serviceuniqueid = new Guid();
            using (SqlCommand cmd = new SqlCommand("Sp_GetServiceUniqueId", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@svcid", svcid);
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        serviceuniqueid = new Guid(rdr["svc_identifier"].ToString());
                    }
                }
            }
            return serviceuniqueid;
        }
        // map tables to service
        public void maptabletoservice(Guid serviceid, int tableid)
        {
            using (SqlCommand cmd = new SqlCommand("Sp_Insertservicetablemapping", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@uniqueid", serviceid);
                cmd.Parameters.AddWithValue("@tableid", tableid);
                cmd.ExecuteNonQuery();
            }
        }

        // map service status
        public void mapservicetostatus(Guid serviceid, int statusid, Guid byUser)
        {
            using (SqlCommand cmd = new SqlCommand("Sp_Insertservicetostatusmapping", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@uniqueid", serviceid);
                cmd.Parameters.AddWithValue("@statusid", statusid);
                cmd.Parameters.AddWithValue("@byuserid", byUser);
                cmd.ExecuteNonQuery();
            }
        }

        // create new servicequeue in database
        public int insertnewservicequeueitem(prop_servicequeue model)
        {
            int serqueitemid = 0;
            using (SqlCommand cmd = new SqlCommand("Sp_InsertServiceQueueItem", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@sq_item", model.sq_item);
                cmd.Parameters.AddWithValue("@sq_quentety", model.sq_quentety);
                if (model.sq_note == null)
                {
                    cmd.Parameters.AddWithValue("@sq_note", string.Empty);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@sq_note", model.sq_note);
                }
                cmd.Parameters.AddWithValue("@sq_service", model.serviceid);
                cmd.Parameters.AddWithValue("@sq_username", model.sq_username);
                cmd.Parameters.AddWithValue("@sq_id", SqlDbType.Int);
                cmd.Parameters["@sq_id"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                serqueitemid = Convert.ToInt32(cmd.Parameters["@sq_id"].Value);

                //string[] custype=model.dsh_custtype;
                //string[] custval=model.dshcustomizationval;
                //for (int i = 0; i < custype.Length;i++ )
                //{
                //    using (SqlCommand cmd1 = new SqlCommand("Sp_InsertSerqueueCustomizationvalMapping", Connection))
                //    {
                //        cmd1.CommandType = CommandType.StoredProcedure;
                //        cmd1.Parameters.AddWithValue("@sqid", serqueitemid);
                //        cmd1.Parameters.AddWithValue("@custtype", Convert.ToInt32(custype[i].ToString()));
                //        cmd1.Parameters.AddWithValue("@custval", Convert.ToInt32(custval[i].ToString()));
                //        cmd1.ExecuteNonQuery();
                //    }
                //}
            }
            return serqueitemid;
        }
        // map servicequeue item to customizatation value

        public List<serviceitem> getservicequeforservice(Guid serviceid)
        {
            List<serviceitem> itemlist = new List<serviceitem>();
            List<serviceitem> tempitemlist = new List<serviceitem>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetServiceQueuelist", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@serviceid", serviceid);
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        serviceitem newitem = new serviceitem();
                        newitem.sqid = Convert.ToInt32(rdr["sq_id"]);
                        newitem.dishname = rdr["dsh_name"].ToString();
                        newitem.extranote = rdr["sq_note"].ToString();
                        newitem.dishquentety = Convert.ToInt32(rdr["sq_quentety"]);
                        tempitemlist.Add(newitem);
                    }
                    rdr.Close();

                    foreach (serviceitem itm in tempitemlist)
                    {
                        itm.cusomization = getcustomizationforqueue(itm.sqid);
                        itemlist.Add(itm);
                    }
                }
            }
            return itemlist;
        }

        private object getcustomizationforqueue(int queueid)
        {
            List<KeyValuePair<string, string>> customization = new List<KeyValuePair<string, string>>();
            using (SqlCommand cmd = new SqlCommand("Sp_getsqervicequecustomization", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@queid", queueid);
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        customization.Add(new KeyValuePair<string, string>(rdr["cst_type"].ToString(), rdr["cst_value"].ToString()));
                    }
                    rdr.Close();
                }
            }
            return customization;
        }

        public List<Guid> getwaiterservicelist(Guid userid)
        {
            List<Guid> servicelist = new List<Guid>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetServiceListForWaiter", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@svc_username",userid);
                using(SqlDataReader rdr=cmd.ExecuteReader())
                {
                    while(rdr.Read())
                    {
                        Guid serid = new Guid(rdr["svc_identifier"].ToString());
                        servicelist.Add(serid);
                    }
                }
            }
            return servicelist;
        }

        public List<Guid> getkitchenstaffrservicelist()
        {
            List<Guid> servicelist = new List<Guid>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetServiceListForKitchenStaff", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        Guid serid = new Guid(rdr["svc_identifier"].ToString());
                        servicelist.Add(serid);
                    }
                }
            }
            return servicelist;
        }

        public List<string> getassignedtables(Guid serviceid)
        {
            List<string> tablelist = new List<string>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetTablelistForService", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@serviceid", serviceid);
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        tablelist.Add(rdr["tbl_number"].ToString());
                    }
                }
            }
            return tablelist;
        }
    }
}
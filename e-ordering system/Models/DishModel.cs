﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace e_ordering_system.Models
{
    public class Prop_DishModel
    {

    }

    public class Prop_dishitems
    {
        public int dsh_id { get; set; }
        public string dsh_name { get; set; }
        public string dsh_prise { get; set; }
        public string[] dsh_custtype { get; set; }
    }

    public class menu
    {
        public int catageryid { get; set; }
        public string catagery { get; set; }
        public List<Prop_Bindmodel> items { get; set; }
    }

    public class Dishmodel : DataConnection
    {
        public int insertdishcatagery(string catagery)
        {
            int id = 0;
            using (SqlCommand cmd = new SqlCommand("Sp_InsertDishcatagery", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@catagery", catagery);
                id = cmd.ExecuteNonQuery();
            }
            return id;
        }

        public List<string> getalldishcatagery()
        {
            List<string> catagerylist = new List<string>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetDishcatageryList", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        catagerylist.Add(rdr["dc_category"].ToString());
                    }
                    rdr.Close();
                }
            }
            return catagerylist;
        }

        public void deletedishcatagery(string catagery)
        {
            using (SqlCommand cmd = new SqlCommand("Sp_DeleteDishCatagery", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@catagery", catagery);
                cmd.ExecuteNonQuery();
            }
        }

        public int insertdishitem(Prop_dishitems itemmodel)
        {
            int id = 0;
            using (SqlCommand cmd = new SqlCommand("Sp_InsertDishItem", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@dsh_id", SqlDbType.Int);
                cmd.Parameters["@dsh_id"].Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@dsh_name", itemmodel.dsh_name);
                cmd.Parameters.AddWithValue("@dsh_prise", itemmodel.dsh_prise);
                cmd.ExecuteNonQuery();
                id = Convert.ToInt32(cmd.Parameters["@dsh_id"].Value);
            }
            return id;
        }

        public int insertdishcustomization(int dishid, int custid)
        {
            int id = 0;
            using (SqlCommand cmd = new SqlCommand("Sp_InsertCustomizationMappping", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@dcm_id", SqlDbType.Int);
                cmd.Parameters["@dcm_id"].Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("@dcm_dishid", dishid);
                cmd.Parameters.AddWithValue("@dcm_custmizationid", custid);
                cmd.ExecuteNonQuery();
                id = Convert.ToInt32(cmd.Parameters["@dcm_id"].Value);
            }
            return id;
        }

        public List<Prop_dishitems> getalldishitems()
        {
            List<Prop_dishitems> itemlist = new List<Prop_dishitems>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetDishItemList", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        Prop_dishitems item = new Prop_dishitems();
                        item.dsh_name = rdr["dsh_name"].ToString();
                        item.dsh_prise = rdr["dsh_prise"].ToString();
                        item.dsh_id = Convert.ToInt32(rdr["dsh_id"]);
                        itemlist.Add(item);
                    }
                    rdr.Close();
                }
            }
            return itemlist;
        }

        public int deletedishitem(int dsh_id)
        {
            using (SqlCommand cmd = new SqlCommand("Sp_DeleteDishItem", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@dsh_id", dsh_id);
                int id = cmd.ExecuteNonQuery();
                return id;
            }
        }

        public Prop_dishitems getdishitem(int dsh_id)
        {
            Prop_dishitems item = new Prop_dishitems();
            using (SqlCommand cmd = new SqlCommand("Sp_getDishItem", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@dsh_id", dsh_id);
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.Read())
                    {
                        item.dsh_name = rdr["dsh_name"].ToString();
                        item.dsh_prise = rdr["dsh_prise"].ToString();
                    }
                    rdr.Close();
                }
                List<string> usercust = new List<string>();
                using (SqlCommand cmd1 = new SqlCommand("Sp_getDishItemCustomization", Connection))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@dsh_id",dsh_id);
                    using (SqlDataReader rdr = cmd1.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            usercust.Add(rdr["cst_type"].ToString());
                        }
                        rdr.Close();
                    }
                }
                item.dsh_custtype = usercust.ToArray();
            }
            return item;
        }

        public int updatedishitem(Prop_dishitems itemmodel)
        {
            using (SqlCommand cmd = new SqlCommand("Sp_UpdateDishItem", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@dsh_id", itemmodel.dsh_id);
                cmd.Parameters.AddWithValue("@dsh_name", itemmodel.dsh_name);
                cmd.Parameters.AddWithValue("@dsh_prise", itemmodel.dsh_prise);
                cmd.ExecuteNonQuery();
            }
            return itemmodel.dsh_id;
        }

        public List<menu> bindmenulist()
        {
            List<menu> menutmplist = new List<menu>();
            List<menu> menulist = new List<menu>();
            using (SqlCommand cmd = new SqlCommand("Sp_GetCatageryForDrop", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                using(SqlDataReader rdr=cmd.ExecuteReader())
                {
                    while(rdr.Read())
                    {
                        menu tmpitm = new menu();
                        tmpitm.catageryid = Convert.ToInt32(rdr["dc_id"].ToString());
                        tmpitm.catagery = rdr["dc_category"].ToString();
                        menutmplist.Add(tmpitm);
                    }
                    rdr.Close();
                }
                foreach(var mnitm in menutmplist)
                {
                    List<Prop_Bindmodel> itmlst = new List<Prop_Bindmodel>();
                    using (SqlCommand cmd1 = new SqlCommand("Sp_GetDishItemForDrop", Connection))
                    {
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@catageryid",mnitm.catageryid);
                        using(SqlDataReader rdr=cmd1.ExecuteReader())
                        {
                            while(rdr.Read())
                            {
                                Prop_Bindmodel itm = new Prop_Bindmodel();
                                itm.modelid = Convert.ToInt32(rdr["dsh_id"]);
                                itm.modelitem = rdr["dsh_name"].ToString();
                                itmlst.Add(itm);
                             }
                        }
                    }
                    mnitm.items = itmlst;
                    menulist.Add(mnitm);
                }
            }
            return menulist;
        }
    }
}